import React, {useEffect, useState} from "react";
import Page from "./Weather_Page";

interface IPos {

        coords: {
            latitude: number,
            longitude: number
        }
    }


const App: React.FC = () => {
    const [state, setState] = useState(true
    )
    const [coordinates, setCoordinates] = useState({
        latitude: 0,
        longitude: 0
    });
    const getPosition = () => {
        navigator.geolocation.getCurrentPosition(showPosition);
    }
    const showPosition = (position: IPos) => {
        console.log(position.coords.latitude);
        console.log(position.coords.longitude);
        setCoordinates(position.coords)
    }

    useEffect(() => {
        getPosition();
    }, [])
    return (
        <div className={"App"}>
            <Page coordinates={coordinates}/>
        </div>
    )
}
export default App;
