import React, {useState} from 'react';
import {makeStyles, createStyles, Theme} from '@material-ui/core/styles';
import {Button} from "@material-ui/core";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            '& > *': {
                margin: theme.spacing(1),
            },
        },
    }),
);

interface Props {
    weather: (city: string)=> void;
}

const SearchForm = ({weather}: Props) => {
    const classes = useStyles();
    const [city, setCity] = useState('');

    const handleChangeComment = (text: string) => {
        setCity(text);
    }

    const getData = () => {
        if (city.length == 0) {
            return false;
        } else {
            weather(city);
            setCity('');
        }
    }

    return (
        <div className={classes.root}>
            <input type='text' value={city} name={'city'}
                   onChange={(event) => handleChangeComment(event.target.value)}/>
            <Button variant="contained" color="primary" href="#contained-buttons" onClick={getData}>Add</Button>
        </div>
    )
}

export default SearchForm;
