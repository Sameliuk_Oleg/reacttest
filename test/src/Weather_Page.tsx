import React, {useEffect, useState} from 'react';
import './App.css';
import SearchForm from "./Search_Form";
import WeatherCity from "./components/WeatherCity";
import {useDispatch, useSelector} from "react-redux";
import {addWeatherEn, addWeatherRu, addWeatherUa} from "./services/actions/action";
import Language from "./components/Language";
import {createStyles, Theme, makeStyles} from '@material-ui/core/styles';
import {Button} from "@material-ui/core";

const API_KEY = '41d74e0ba254d676f4a05db7b3e7c82a'

interface IProps {
    coordinates: {
        latitude: number,
        longitude: number

    }
}

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        margin: {
            margin: theme.spacing(1),
        },
        extendedIcon: {
            marginRight: theme.spacing(1),
        },
    }),
);

const Page: ({coordinates}: IProps) => JSX.Element = ({coordinates}: IProps) => {
    const allCityEn = useSelector((state: any) => state.weather.weatherEn);
    const allCityRu = useSelector((state: any) => state.weather.weatherRu);
    const allCityUa = useSelector((state: any) => state.weather.weatherUa);
    const dispatch = useDispatch();
    let dataWeather;
    const [lang, setLang] = useState(localStorage.getItem('lang'));
    const changeLang = (language: string) => {
        setLang(language);
        localStorage.setItem('lang', language);
    }

    const getWeather = async (city: string) => {
        const api_url_en = await fetch(
            `https://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${API_KEY}&lang=en`);
        const dataEn = await api_url_en.json();
        if (allCityEn.some((c: any) => c.name === dataEn.name)) {
            return false;
        } else {
            dispatch(addWeatherEn([dataEn]));
        }
        const api_url_ua = await fetch(
            `https://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${API_KEY}&lang=ua`);
        const dataUa = await api_url_ua.json();
        if (allCityUa.some((c: any) => c.name === dataUa.name)) {
            return false;
        } else {
            dispatch(addWeatherUa([dataUa]));
        }
        const api_url_ru = await fetch(
            `https://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${API_KEY}&lang=ru`);
        const dataRu = await api_url_ru.json();
        if (allCityRu.some((c: any) => c.name === dataRu.name)) {
            return false;
        } else {
            dispatch(addWeatherRu([dataRu]));
        }
    }

    const getWeatherLocation = async () => {
        const api_url_en = await fetch(
            `https://api.openweathermap.org/data/2.5/weather?lat=${coordinates.latitude}&lon=${coordinates.longitude}&appid=${API_KEY}&lang=en`);
        const dataEn = await api_url_en.json();

        if (allCityEn.some((c: any) => c.name === dataEn.name)) {
            return false
        } else {
            dispatch(addWeatherEn([dataEn]))
        }
        const api_url_ua = await fetch(
            `https://api.openweathermap.org/data/2.5/weather?lat=${coordinates.latitude}&lon=${coordinates.longitude}&appid=${API_KEY}&lang=ua`);
        const dataUa = await api_url_ua.json();

        if (allCityUa.some((c: any) => c.name === dataUa.name)) {
            return false
        } else {
            dispatch(addWeatherUa([dataUa]))
        }
        const api_url_ru = await fetch(
            `https://api.openweathermap.org/data/2.5/weather?lat=${coordinates.latitude}&lon=${coordinates.longitude}&appid=${API_KEY}&lang=ru`);
        const dataRu = await api_url_ru.json();

        if (allCityRu.some((c: any) => c.name === dataRu.name)) {
            return false
        } else {
            dispatch(addWeatherRu([dataRu]))
        }
    }

    if (lang == 'EN') {
        dataWeather = allCityEn;
    } else {
        if (lang == 'UK') {
            dataWeather = allCityUa
        } else dataWeather = allCityRu
    }
    const classes = useStyles();
    return (
        <div>
            <Language changeLang={changeLang}/>
            <SearchForm weather={getWeather}/>
            <Button variant="contained" color="primary" href="#contained-buttons" onClick={getWeatherLocation}
                    className={classes.margin}>Get location weather</Button>
            <WeatherCity data={dataWeather}/>
        </div>
    )
}

export default Page;
