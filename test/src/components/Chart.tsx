import React from "react";
import ReactApexChart from "react-apexcharts";
import {IWeather} from "../controlers/typedef";

interface IChart {
    dataChart: IWeather
}

export const ApexChart = ({dataChart}: IChart) => {
    let color
    if (Math.floor(dataChart.main.temp_max - 273) < 0) {
        color = "#5B8CFF";
    } else {
        color = "#FF715B";
    }

    const state = {
        series: [{
            data: [Math.floor(dataChart.main.temp_min - 273), Math.floor(dataChart.main.temp - 273),
                Math.floor(dataChart.main.temp_max - 273), Math.floor(dataChart.main.temp - 273),
                Math.floor(dataChart.main.temp_min - 273)]
        }],
        options: {
            chart: {
                height: 150,
                type: 'area',
                zoom: {
                    enabled: false
                }
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                curve: 'straight'
            },
            title: {
                align: 'left'
            },
            grid: {
                row: {
                    colors: ['#f3f3f3', 'transparent'],
                    opacity: 0.5
                },
            },
            xaxis: {
                categories: ['night', 'morning', 'day', 'evening', 'night'],
            }
        },
    };
    return (
        <div id="chart">
            <ReactApexChart options={state.options} series={state.series} type="area" height={150} stop-color={color}/>
        </div>
    )
}
