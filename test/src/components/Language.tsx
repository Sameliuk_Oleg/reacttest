import React from 'react';

const languages = ['EN', 'RU', 'UK'];
const Language = ({changeLang}: any) => {
    const defLang = localStorage.getItem('lang');
    return (
        <div>
            <select name="language" onChange={(event) => changeLang(event.target.value)} className={"right"}>
                {languages.map((lang: string) => <option
                    selected={defLang === lang ? true : false}>{lang.toLocaleUpperCase()}</option>)}
            </select>
        </div>
    )
}

export default Language;


