import React, {useState} from 'react';
import {IWeather} from "../controlers/typedef";
import {makeStyles} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import {ApexChart} from "./Chart";

interface IProps {
    data: Array<IWeather>
}

const useStyles = makeStyles({
    root: {
        maxWidth: 345,
    },
    media: {
        height: 140,
    }

});

const CardWeatherCity = ({data}: IProps) => {
    const classes = useStyles();
    const date = new Date();
    const [measure, setMeasure] = useState({
        measure: 'C',
        index: -1
    });

    const changeMeasure = (type: string, index: number) => {
        setMeasure({
            measure: type,
            index: index
        })
    }
    const toCelsius = (temp: number) => {
        return (Math.floor(Number(temp) - 273));
    }
    const toFaren = (temp: number) => {
        return (Math.floor((Number(temp) * (9 / 5)) - 459.67));
    }
    return (
        <div className={'display_position'}>
            {data.length != 0 ?
                data.map((city: IWeather, index: number) => (
                    <Card className={classes.root}>
                        <CardActionArea>
                            <CardContent className="card-content-position">
                                <Typography variant="body2" color="textSecondary" component="p">
                                    {city.weather[0].main}
                                </Typography>
                            </CardContent>
                            <CardContent className="card-content-temp">
                                <Typography gutterBottom variant="h5" component="h2">
                                    {city.name}
                                </Typography>
                                <Typography variant="body2" color="textSecondary" component="p">
                                    {date.toUTCString()}
                                </Typography>
                            </CardContent>
                            <CardMedia
                                className={classes.media}
                                image="/static/images/cards/contemplative-reptile.jpg"
                                title="Contemplative Reptile"
                            />
                            <ApexChart dataChart={city}/>
                            <CardContent className="card-content-temp">
                                {(measure.index == index && measure.measure === 'C') ?
                                    <Typography gutterBottom variant="h5" component="h2">
                                        {toCelsius(city.main.temp)} <span> C | </span>
                                        <button
                                            onClick={() => changeMeasure('F', index)} className={"temperature"}> F
                                        </button>
                                        <Typography variant="body2" color="textSecondary" component="p">
                                            Feels like: {toCelsius(city.main.feels_like)}
                                        </Typography>
                                    </Typography> : (measure.index == index && measure.measure === 'F') ?
                                        <Typography gutterBottom variant="h5" component="h2">
                                            {toFaren(city.main.temp)}
                                            <button
                                                onClick={() => changeMeasure('C', index)} className={"temperature"}>C
                                            </button>
                                            <span> | F </span>
                                            <Typography variant="body2" color="textSecondary" component="p">
                                                Feels like: {toFaren(city.main.feels_like)}
                                            </Typography>
                                        </Typography> : <Typography gutterBottom variant="h5" component="h2">
                                            {toFaren(city.main.temp)}
                                            <button
                                                onClick={() => changeMeasure('C', index)} className={"temperature"}>C
                                            </button>
                                            <span> | F </span>
                                            <Typography variant="body2" color="textSecondary" component="p">
                                                Feels like: {toFaren(city.main.feels_like)}
                                            </Typography>
                                        </Typography>}
                            </CardContent>
                            <CardContent className="card-content-position">
                                <Typography variant="body2" color="textSecondary" component="p">
                                    Wind: {city.wind.speed} m/s
                                </Typography>
                                <Typography variant="body2" color="textSecondary" component="p">
                                    Humidity: {city.main.humidity}%
                                </Typography>
                                <Typography variant="body2" color="textSecondary" component="p">
                                    Pressure: {city.main.pressure}Pa
                                </Typography>
                            </CardContent>
                        </CardActionArea>
                    </Card>))

                : <p>Please, enter your city</p>
            }
        </div>
    );
}


export default CardWeatherCity;
