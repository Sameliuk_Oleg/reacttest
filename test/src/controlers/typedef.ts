export interface IWeather {
    main: { temp: number, feels_like: number, humidity: string, pressure: string, temp_min: number, temp_max: number }
    name: string
    wind: { speed: string }
    weather: Array<{ main: string }>
}

