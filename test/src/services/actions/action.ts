import {IWeather} from "../../controlers/typedef";

export const GET_WEATHER_EN = 'GET_WEATHER_EN';
export const GET_WEATHER_UA = 'GET_WEATHER_UA';
export const GET_WEATHER_RU = 'GET_WEATHER_RU';

export const addWeatherEn = (weather: Array<IWeather>) => {
    return{
        type: GET_WEATHER_EN,
        payload: weather
    }
}
export const addWeatherUa = (weather:  Array<IWeather>) => {
    return{
        type: GET_WEATHER_UA,
        payload: weather
    }
}
export const addWeatherRu = (weather:  Array<IWeather>) => {
    return{
        type: GET_WEATHER_RU,
        payload: weather
    }
}
