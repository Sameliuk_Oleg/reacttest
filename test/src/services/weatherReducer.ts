import {GET_WEATHER_EN, GET_WEATHER_RU, GET_WEATHER_UA} from "./actions/action";
import {IWeather} from "../controlers/typedef";

interface IState {
    weatherEn: Array<IWeather>,
    weatherUa: Array<IWeather>,
    weatherRu: Array<IWeather>
}

const initialState: IState = {
    weatherEn: [],
    weatherUa: [],
    weatherRu: []
}

export const weatherReducer = (state = initialState, action: {
    type: string,
    payload: Array<IWeather>
}) => {
    switch (action.type) {
        case GET_WEATHER_EN:
            return {...state, weatherEn: state.weatherEn.concat(action.payload)};
        case GET_WEATHER_UA:
            return {...state, weatherUa: state.weatherUa.concat(action.payload)};
        case GET_WEATHER_RU:
            return {...state, weatherRu: state.weatherRu.concat(action.payload)};
        default:
            return state;
    }
    return state;
}
